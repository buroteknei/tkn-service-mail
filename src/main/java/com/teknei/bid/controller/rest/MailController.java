package com.teknei.bid.controller.rest;

import com.teknei.bid.dto.MailRequestDTO;
import com.teknei.bid.dto.MailResponseDTO;
import com.teknei.bid.dto.OTPVerificationDTO;
import com.teknei.bid.persistence.entities.BidClieRegEsta;
import com.teknei.bid.persistence.entities.BidEstaProc;
import com.teknei.bid.persistence.entities.BidOtp;
import com.teknei.bid.persistence.repository.BidClieRegEstaRepository;
import com.teknei.bid.persistence.repository.BidEstaProcRepository;
import com.teknei.bid.persistence.repository.BidOtpRepository;
import com.teknei.bid.services.SendMailTask;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/mail")
public class MailController {

    @Autowired
    private BidOtpRepository otpRepository;
    @Autowired
    private SendMailTask mailTask;
    @Autowired
    private BidEstaProcRepository bidEstaProcRepository;
    @Autowired
    private BidClieRegEstaRepository bidClieRegEstaRepository;
    private static final String ESTA_PROC_OTP_REGISTERED = "ENV-REGOTP";
    private static final String ESTA_PROC_OTP_VALIDATED = "AUT-OTP";

    private static final Logger log = LoggerFactory.getLogger(MailController.class);

    @ApiOperation(value = "Sends the generated contract for the related customer", response = String.class)
    @RequestMapping(value = "/contractSigned", method = RequestMethod.POST)
    public ResponseEntity<String> sendContract(@RequestBody MailRequestDTO mailRequestDTO) {
        try {
            mailTask.sendMailContract(mailRequestDTO.getIdClient());
            return new ResponseEntity<>("OK", HttpStatus.OK);
        } catch (MessagingException e) {
            log.error("Error sending mail with contract to customerId: {} with message: {}", mailRequestDTO, e.getMessage());
            return new ResponseEntity<>("ERROR", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Generates an OTP related to the customer", response = MailResponseDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the OTP could be saved successfully"),
            @ApiResponse(code = 422, message = "If the OTP could not be saved in relation to the customer")
    })
    @RequestMapping(value = "/OTP/resend", method = RequestMethod.POST)
    public ResponseEntity<MailResponseDTO> reGenerateOtp(@RequestBody MailRequestDTO mailRequestDTO) {
        try {
            List<BidOtp> bidOtpList = otpRepository.findByIdClie(mailRequestDTO.getIdClient());
            for (BidOtp o :
                    bidOtpList) {
                o.setIdEsta(2);
                o.setFchModi(new Timestamp(System.currentTimeMillis()));
                o.setUsrOpeModi(mailRequestDTO.getUsername());
                otpRepository.save(o);
            }
        } catch (Exception e) {
            log.error("Error deactivating previous OTP: {}", e.getMessage());
        }
        ResponseEntity<MailResponseDTO> primaryResponse = generateOTP(mailRequestDTO);
        return primaryResponse;
    }

    @ApiOperation(value = "Generates an OTP related to the customer", response = MailResponseDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the OTP could be saved successfully"),
            @ApiResponse(code = 422, message = "If the OTP could not be saved in relation to the customer")
    })
    @RequestMapping(value = "/OTP", method = RequestMethod.POST)
    public ResponseEntity<MailResponseDTO> generateOTP(@RequestBody MailRequestDTO mailRequestDTO) {
        BidOtp otp = new BidOtp();
        otp.setFchCrea(new Timestamp(System.currentTimeMillis()));
        otp.setIdClie(mailRequestDTO.getIdClient());
        otp.setOtp(generatePin());
        otp.setUsed(false);
        otp.setSend(false);
        otp.setUsrCrea(mailRequestDTO.getUsername());
        otp.setUsrOpeCrea(mailRequestDTO.getUsername());
        otp.setIdEsta(1);
        otp.setIdTipo(3);
        try {
            otpRepository.save(otp);
            updateStatus(mailRequestDTO.getIdClient(), ESTA_PROC_OTP_REGISTERED, mailRequestDTO.getUsername());
        } catch (Exception e) {
            log.error("Error saving otp for: {} with message: {}", mailRequestDTO, e.getMessage());
            return new ResponseEntity<>((MailResponseDTO) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
        MailResponseDTO responseDTO = new MailResponseDTO();
        responseDTO.setOtp(otp.getOtp());
        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }

    @ApiOperation(value = "Generates an OTP related to the customer. The format is alpha-numeric and it must be sent in mail", response = MailResponseDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the OTP could be saved successfully"),
            @ApiResponse(code = 422, message = "If the OTP could not be saved in relation to the customer")
    })
    @RequestMapping(value = "/OTP/vc", method = RequestMethod.POST)
    public ResponseEntity<MailResponseDTO> generateOTPaz(@RequestBody MailRequestDTO mailRequestDTO) {
        BidOtp otp = new BidOtp();
        otp.setFchCrea(new Timestamp(System.currentTimeMillis()));
        otp.setIdClie(mailRequestDTO.getIdClient());
        otp.setOtp(generateAlphanumeric());
        otp.setUsed(false);
        otp.setSend(false);
        otp.setUsrCrea(mailRequestDTO.getUsername());
        otp.setUsrOpeCrea(mailRequestDTO.getUsername());
        otp.setIdEsta(1);
        otp.setIdTipo(3);
        try {
            otpRepository.save(otp);
            updateStatus(mailRequestDTO.getIdClient(), ESTA_PROC_OTP_REGISTERED, mailRequestDTO.getUsername());
        } catch (Exception e) {
            log.error("Error saving otp for: {} with message: {}", mailRequestDTO, e.getMessage());
            return new ResponseEntity<>((MailResponseDTO) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
        MailResponseDTO responseDTO = new MailResponseDTO();
        responseDTO.setOtp(otp.getOtp());
        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }



    @ApiOperation(value = "Validates OTP and uses it", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Validation successfull"),
            @ApiResponse(code = 404, message = "No OTP found"),
            @ApiResponse(code = 422, message = "Error validating OTP")
    })
    @RequestMapping(value = "/validateOTP", method = RequestMethod.POST)
    public ResponseEntity<String> validateOTP(@RequestBody OTPVerificationDTO dto) {
        try {
            List<BidOtp> list = otpRepository.findByIdClieAndOtpAndUsed(dto.getIdClient(), dto.getOtp(), false);
            if (list == null || list.isEmpty()) {
                return new ResponseEntity<>((String) null, HttpStatus.NOT_FOUND);
            }
            BidOtp target = list.get(0);
            target.setUsed(true);
            target.setFchModi(new Timestamp(System.currentTimeMillis()));
            target.setUsrModi(dto.getUsername());
            target.setUsrOpeModi(dto.getUsername());
            target.setIdEsta(2);
            otpRepository.save(target);
            updateStatus(dto.getIdClient(), ESTA_PROC_OTP_VALIDATED, dto.getUsername());
            return new ResponseEntity<>("OK", HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error validating OTP for request:{} with message: {}", dto, e.getMessage());
            return new ResponseEntity<>((String) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    private String generatePin() {
        SecureRandom random = new SecureRandom();
        int num = random.nextInt(1000000);
        String formatted = String.format("%06d", num);
        return formatted;
    }

    private String generateAlphanumeric(){
        String random = UUID.randomUUID().toString();
        return random;
    }

    private void updateStatus(Long idClient, String status, String username) {
        try {
            BidEstaProc estaProc = bidEstaProcRepository.findTopByCodEstaProcAndIdEsta(status, 1);
            BidClieRegEsta regEsta = bidClieRegEstaRepository.findByIdClieAndIdEstaProc(idClient, estaProc.getIdEstaProc());
            if (regEsta == null) {
                log.warn("Status for process: {} found null", idClient);
            }
            regEsta.setEstaConf(true);
            regEsta.setFchModi(new Timestamp(System.currentTimeMillis()));
            regEsta.setUsrModi(username);
            regEsta.setUsrOpeModi(username);
            bidClieRegEstaRepository.save(regEsta);
        } catch (Exception e) {
            log.error("Error finding status of process for customer: {} with message: {}", idClient, e.getMessage());
        }
    }


}