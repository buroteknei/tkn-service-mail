package com.teknei.bid.util;

import java.util.HashMap;
import java.util.Map;

public enum ErrorSmsCode {

    API_USER_NOT_INCLUDED(101), API_PASSWORD_NOT_INCLUDED(102), API_TEXT_NOT_INCLUDED(103), API_NUMBER_NOT_INCLUDED(104), API_SIZE_EXXEDED(105), NETWORK_ERROR_1(109), NETWORK_ERROR_2(110), PROVIDER_SYSTEM_ERROR_1(200), INVALID_CREDENTIALS(201), INVALID_IP(202), MAX_MESSAGES_REACHED(204), TRAFFIC_NOT_ALLOWED(205), PROVIDER_ERROR_SENDING(254), PROVIDER_SYSTEM_ERROR_2(305), PROVIDER_SYSTEM_ERROR(306), INVALID_NUMBER(307), INTERNAL_API_NOT_NUMBER_PROVIDED(10001), INTERNAL_API_NUMBER_NOT_VALID(10002), INTERNAL_API_UNKNOWN_ERROR(10003);

    private ErrorSmsCode(int code) {
        this.code = code;
        Holder.MAP.put(code, this);
    }

    private int code;


    public int getCode() {
        return code;
    }

    private static class Holder {

        static Map<Integer, ErrorSmsCode> MAP = new HashMap<>();
    }

    public static ErrorSmsCode find(int code) {
        ErrorSmsCode t = Holder.MAP.get(code);
        if (t == null) {
            throw new IllegalStateException(String.format("Unsupported type %s.", code));
        }
        return t;
    }


}